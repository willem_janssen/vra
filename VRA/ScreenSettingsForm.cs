﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VRA {
	public partial class ScreenSettingsForm : Form {
		public MainForm MainForm { get; set; }

		public ScreenSettingsForm() {
			InitializeComponent();
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

		}

		private void SettingsForm_Enter(object sender, EventArgs e) {
		}

		private void SettingsForm_Leave(object sender, EventArgs e) {
		}

		private void SettingsForm_Activated(object sender, EventArgs e) {
			//foreach (ViewForm form in MainForm.ViewForms) {
			//	form.GetNameLabel().Visible = true;
			//	Size size = form.Size;
			//	form.FormBorderStyle = FormBorderStyle.Sizable;
			//	form.Size = size;
			//}
		}

		private void SettingsForm_Deactivate(object sender, EventArgs e) {
			//foreach (ViewForm form in MainForm.ViewForms) {
			//	form.GetNameLabel().Visible = false;
			//	Size size = form.Size;
			//	form.FormBorderStyle = FormBorderStyle.None;
			//	form.Size = size;
			//}
		}

		private void SettingsForm_Load(object sender, EventArgs e) {
			foreach (ViewForm form in MainForm.ViewForms) {
				form.GetNameLabel().Visible = true;
				Size size = form.Size;
				form.FormBorderStyle = FormBorderStyle.Sizable;
				form.Size = size;
				form.TopMost = false;
				form.Show();
			}
		}

		private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e) {
			foreach (ViewForm form in MainForm.ViewForms) {
				form.GetNameLabel().Visible = false;
				Size size = form.Size;
				form.FormBorderStyle = FormBorderStyle.None;
				form.Size = size;
				form.Hide();
			}
		}

		private void OKButton_Click(object sender, EventArgs e) {
			Close();
			
			Properties.Settings.Default.LeftFormBounds = MainForm.LeftForm.Bounds;
			Properties.Settings.Default.RightFormBounds = MainForm.RightForm.Bounds;
			Properties.Settings.Default.Save();
		}

		private void CloseButton_Click(object sender, EventArgs e) {
			Close();
		}
	}
}
