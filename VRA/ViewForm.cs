﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace VRA {
	public partial class ViewForm : Form {
		public MainForm MainForm;

		// The filename of the movie we want to show next
		string QueuedMedia;

		// If the movie should play now/as quickly as possible
		bool ShouldPlay = false;

		public ViewForm() {
			// Generated function call, to set up buttons etc. as designed
			InitializeComponent();
		}

		// Public accessor to the big label indicating if this is the left or the right form
		public Label GetNameLabel() {
			return NameLabel;
		}

		private void ViewForm_Load(object sender, EventArgs e) {
			// Make sure the name label covers all and is centered
			NameLabel.Dock = DockStyle.Fill;

			// Hide all the Windows Media Player buttons
			MediaPlayer.uiMode = "none";

			// Make media player fill the whole window
			MediaPlayer.stretchToFit = true;
		}

		public void QueueMedia(string inName) {
			// Prepare the next movie
			Console.WriteLine("Queueing " + inName + " on " + Text);

			// Store the desired filename
			QueuedMedia = inName;

			// And load it into the media player if it isn't showing a movie right now, or about to show one
			if (!IsPlaying && !ShouldPlay)
				LoadMedia(QueuedMedia);
		}

		void LoadMedia(string inName) {
			// Actually load the queued movie into the media player
			Console.WriteLine("Loading " + inName + " on " + Text);
			Debug.Assert(inName != "");

			// We're only queueing preemptively, we don't want to play right away
			ShouldPlay = false;

			// Make sure we don't see the first frame, but the screen remains black
			MediaPlayer.Hide();

			//MediaPlayer.settings.autoStart = false;

			// Load the movie
			MediaPlayer.URL = inName;

			// Turn off sound if so desired
			MediaPlayer.settings.mute = Properties.Settings.Default.Mute;
		}

		public void Play(string inName) {
			// Start playing the queued movie
			Console.WriteLine("Playing " + inName + " on " + Text);
			Debug.Assert(inName.Equals(QueuedMedia));

			// We should be playing now
			ShouldPlay = true;

			// Unhide movie
			MediaPlayer.Show();

			// Press play button
			MediaPlayer.Ctlcontrols.play();

			// Clear the queue
			QueuedMedia = "";
		}

		// Is the movie playing right now
		public bool IsPlaying {
			get {
				return MediaPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying;
			}
		}

		// Stop playing the movie
		public void Stop() {
			// Unload movie in media player
			MediaPlayer.URL = "";

			// Load next queued movie
			LoadMedia(QueuedMedia);
		}

		private void LeftForm_FormClosing(object sender, FormClosingEventArgs e) {
			// Make sure this form can't be closed by accident, only by quitting the program
			e.Cancel = true;
		}

		// Callback from the media player when its state changes
		private void MediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e) {
			// Magical Windows Media Player constants
			const int playing = 3;
			const int stopped = 1;

			Console.WriteLine("PlayState changed to " + e.newState.ToString() + " on " + Text);

			// Intercept the auto-play when the movie is done loading
			if (e.newState == playing && !ShouldPlay) {
				Console.WriteLine("Finished loading " + " on " + Text);

				// Pause the movie until we really want to play it
				MediaPlayer.Ctlcontrols.pause();
			}

			// Load next movie when this one is done
			if (e.newState == stopped)
				LoadMedia(QueuedMedia);
		}

		private void ViewForm_KeyPress(object sender, KeyPressEventArgs e) {
			// Pressing Escape (when this window has focus) stops VRA
			if (e.KeyChar == (char)Keys.Escape)
				MainForm.Stop();
		}

		private void ViewForm_VisibleChanged(object sender, EventArgs e) {
			if (!Visible) {
				Console.WriteLine(Text + " hidden");
				// Stop playing when window is hidden, we probably just stopped VRA
				// We don't call Stop, since we don't want to load the next movie
				MediaPlayer.URL = "";
				ShouldPlay = false;
			}
		}
	}
}
