﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VRA {
	public partial class PreferencesForm : Form {
		public Keys LeftKey = Keys.F1;
		public Keys RightKey = Keys.F2;

		bool ListeningForLeftKey = false;
		bool ListeningForRightKey = false;

		public PreferencesForm() {
			InitializeComponent();
		}

		private void LeftKeyTextBox_MouseClick(object sender, MouseEventArgs e) {
			LeftKeyTextBox.Text = "Druk op een toets";
			ListeningForLeftKey = true;
		}

		private void PreferencesForm_Load(object sender, EventArgs e) {
			LeftKeyTextBox.Text = LeftKey.ToString();
			RightKeyTextBox.Text = RightKey.ToString();
		}

		private void PreferencesForm_KeyDown(object sender, KeyEventArgs e) {
			if (ListeningForLeftKey) {
				LeftKey = e.KeyCode;
				ListeningForLeftKey = false;
				LeftKeyTextBox.Text = LeftKey.ToString();
			}
			if (ListeningForRightKey) {
				RightKey = e.KeyCode;
				ListeningForRightKey = false;
				RightKeyTextBox.Text = RightKey.ToString();
			}
		}

		private void RightKeyTextBox_MouseClick(object sender, MouseEventArgs e) {
			RightKeyTextBox.Text = "Druk op een toets";
			ListeningForRightKey = true;
		}
	}
}
