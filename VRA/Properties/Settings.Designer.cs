﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VRA.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0, 0, 300, 300")]
        public global::System.Drawing.Rectangle LeftFormBounds {
            get {
                return ((global::System.Drawing.Rectangle)(this["LeftFormBounds"]));
            }
            set {
                this["LeftFormBounds"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("300, 0, 300, 300")]
        public global::System.Drawing.Rectangle RightFormBounds {
            get {
                return ((global::System.Drawing.Rectangle)(this["RightFormBounds"]));
            }
            set {
                this["RightFormBounds"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public string[] MediaFolders {
            get {
                return ((string[])(this["MediaFolders"]));
            }
            set {
                this["MediaFolders"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("F1")]
        public global::System.Windows.Forms.Keys LeftKey {
            get {
                return ((global::System.Windows.Forms.Keys)(this["LeftKey"]));
            }
            set {
                this["LeftKey"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("F2")]
        public global::System.Windows.Forms.Keys RightKey {
            get {
                return ((global::System.Windows.Forms.Keys)(this["RightKey"]));
            }
            set {
                this["RightKey"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Mute {
            get {
                return ((bool)(this["Mute"]));
            }
            set {
                this["Mute"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string LastFolder {
            get {
                return ((string)(this["LastFolder"]));
            }
            set {
                this["LastFolder"] = value;
            }
        }
    }
}
