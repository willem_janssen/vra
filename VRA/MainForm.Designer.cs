﻿namespace VRA {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.MediaFoldersList = new System.Windows.Forms.ListBox();
			this.FileList = new System.Windows.Forms.ListBox();
			this.ExplanationLabel = new System.Windows.Forms.Label();
			this.AddFolderButton = new System.Windows.Forms.Button();
			this.MediaFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.StartStopButton = new System.Windows.Forms.Button();
			this.RemoveFolderButton = new System.Windows.Forms.Button();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.ConfigurationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ScreenSettingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.PreferencesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.overToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// MediaFoldersList
			// 
			this.MediaFoldersList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.MediaFoldersList.FormattingEnabled = true;
			this.MediaFoldersList.ItemHeight = 25;
			this.MediaFoldersList.Location = new System.Drawing.Point(24, 110);
			this.MediaFoldersList.Margin = new System.Windows.Forms.Padding(6);
			this.MediaFoldersList.Name = "MediaFoldersList";
			this.MediaFoldersList.Size = new System.Drawing.Size(814, 204);
			this.MediaFoldersList.TabIndex = 0;
			this.MediaFoldersList.SelectedIndexChanged += new System.EventHandler(this.MediaFoldersList_SelectedIndexChanged);
			// 
			// FileList
			// 
			this.FileList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FileList.FormattingEnabled = true;
			this.FileList.ItemHeight = 25;
			this.FileList.Location = new System.Drawing.Point(26, 398);
			this.FileList.Margin = new System.Windows.Forms.Padding(6);
			this.FileList.Name = "FileList";
			this.FileList.Size = new System.Drawing.Size(1016, 279);
			this.FileList.TabIndex = 1;
			this.FileList.SelectedIndexChanged += new System.EventHandler(this.FileList_SelectedIndexChanged);
			// 
			// ExplanationLabel
			// 
			this.ExplanationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ExplanationLabel.Location = new System.Drawing.Point(24, 687);
			this.ExplanationLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.ExplanationLabel.Name = "ExplanationLabel";
			this.ExplanationLabel.Size = new System.Drawing.Size(1022, 81);
			this.ExplanationLabel.TabIndex = 3;
			this.ExplanationLabel.Text = "Druk op [left_key] of [right_key] om op de linker of rechter monitor een afbeeldi" +
    "ng of filmpje te laten zien";
			this.ExplanationLabel.Visible = false;
			// 
			// AddFolderButton
			// 
			this.AddFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.AddFolderButton.Location = new System.Drawing.Point(854, 110);
			this.AddFolderButton.Margin = new System.Windows.Forms.Padding(6);
			this.AddFolderButton.Name = "AddFolderButton";
			this.AddFolderButton.Size = new System.Drawing.Size(192, 44);
			this.AddFolderButton.TabIndex = 4;
			this.AddFolderButton.Text = "Voeg &map toe";
			this.AddFolderButton.UseVisualStyleBackColor = true;
			this.AddFolderButton.Click += new System.EventHandler(this.AddFolderButton_Click);
			// 
			// MediaFolderBrowserDialog
			// 
			this.MediaFolderBrowserDialog.ShowNewFolderButton = false;
			// 
			// StartButton
			// 
			this.StartStopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.StartStopButton.Location = new System.Drawing.Point(26, 773);
			this.StartStopButton.Margin = new System.Windows.Forms.Padding(6);
			this.StartStopButton.Name = "StartButton";
			this.StartStopButton.Size = new System.Drawing.Size(202, 44);
			this.StartStopButton.TabIndex = 5;
			this.StartStopButton.Text = "&Start";
			this.StartStopButton.UseVisualStyleBackColor = true;
			this.StartStopButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// RemoveFolderButton
			// 
			this.RemoveFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.RemoveFolderButton.Location = new System.Drawing.Point(854, 167);
			this.RemoveFolderButton.Margin = new System.Windows.Forms.Padding(6);
			this.RemoveFolderButton.Name = "RemoveFolderButton";
			this.RemoveFolderButton.Size = new System.Drawing.Size(192, 44);
			this.RemoveFolderButton.TabIndex = 6;
			this.RemoveFolderButton.Text = "&Verwijder map";
			this.RemoveFolderButton.UseVisualStyleBackColor = true;
			this.RemoveFolderButton.Click += new System.EventHandler(this.RemoveFolderButton_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ConfigurationMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(12, 4, 0, 4);
			this.menuStrip1.Size = new System.Drawing.Size(1070, 44);
			this.menuStrip1.TabIndex = 7;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// ConfigurationMenuItem
			// 
			this.ConfigurationMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ScreenSettingsMenuItem,
            this.PreferencesMenuItem});
			this.ConfigurationMenuItem.Name = "ConfigurationMenuItem";
			this.ConfigurationMenuItem.Size = new System.Drawing.Size(151, 36);
			this.ConfigurationMenuItem.Text = "&Instellingen";
			// 
			// ScreenSettingsMenuItem
			// 
			this.ScreenSettingsMenuItem.Name = "ScreenSettingsMenuItem";
			this.ScreenSettingsMenuItem.Size = new System.Drawing.Size(360, 38);
			this.ScreenSettingsMenuItem.Text = "Pas &vensterposities aan";
			this.ScreenSettingsMenuItem.Click += new System.EventHandler(this.pasvensterpositiesAanToolStripMenuItem_Click);
			// 
			// PreferencesMenuItem
			// 
			this.PreferencesMenuItem.Name = "PreferencesMenuItem";
			this.PreferencesMenuItem.Size = new System.Drawing.Size(360, 38);
			this.PreferencesMenuItem.Text = "&Voorkeuren";
			this.PreferencesMenuItem.Click += new System.EventHandler(this.PreferencesMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.overToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
			this.helpToolStripMenuItem.Text = "&Help";
			// 
			// overToolStripMenuItem
			// 
			this.overToolStripMenuItem.Name = "overToolStripMenuItem";
			this.overToolStripMenuItem.Size = new System.Drawing.Size(166, 38);
			this.overToolStripMenuItem.Text = "&Over";
			this.overToolStripMenuItem.Click += new System.EventHandler(this.overToolStripMenuItem_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 79);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(117, 25);
			this.label1.TabIndex = 8;
			this.label1.Text = "Media sets";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 367);
			this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(252, 25);
			this.label2.TabIndex = 9;
			this.label2.Text = "Filmpjes en afbeeldingen";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1070, 840);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.RemoveFolderButton);
			this.Controls.Add(this.StartStopButton);
			this.Controls.Add(this.AddFolderButton);
			this.Controls.Add(this.ExplanationLabel);
			this.Controls.Add(this.FileList);
			this.Controls.Add(this.MediaFoldersList);
			this.Controls.Add(this.menuStrip1);
			this.KeyPreview = true;
			this.MainMenuStrip = this.menuStrip1;
			this.Margin = new System.Windows.Forms.Padding(6);
			this.Name = "MainForm";
			this.Text = "VRA Media Viewer";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox MediaFoldersList;
		private System.Windows.Forms.ListBox FileList;
		private System.Windows.Forms.Label ExplanationLabel;
		private System.Windows.Forms.Button AddFolderButton;
		private System.Windows.Forms.FolderBrowserDialog MediaFolderBrowserDialog;
		private System.Windows.Forms.Button StartStopButton;
		private System.Windows.Forms.Button RemoveFolderButton;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem ConfigurationMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ScreenSettingsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem overToolStripMenuItem;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ToolStripMenuItem PreferencesMenuItem;
	}
}

