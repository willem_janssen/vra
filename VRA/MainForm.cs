﻿using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;// for DllImportAttribute


namespace VRA {
	// This is the form you see when starting, where movies can be selected and the program can be started
	public partial class MainForm : Form {

		// Import global hotkey functions, for starting and stopping movies even when the program does not have focus
		[DllImport("user32.dll")]
		public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);

		[DllImport("user32.dll")]
		public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

		// Redefine windows constants
		private const int WM_HOTKEY = 0x312;
		private const int HOTKEY_LEFT = 1;
		private const int HOTKEY_RIGHT = 2;

		// The form containing the settings for the movie viewer windows
		ScreenSettingsForm ScreenSettingsForm;

		// The two forms holding the movie/image viewers
		public ViewForm[] ViewForms = new ViewForm[2];

		// Aliases for the left and right forms
		public ViewForm LeftForm;
		public ViewForm RightForm;

		// If we're running right now
		public bool IsStarted = false;

		public MainForm() {
			// Initialize visual components, as set up in the designer
			InitializeComponent();

			// Create movie view forms
			ViewForms[0] = new ViewForm();
			ViewForms[1] = new ViewForm();
			LeftForm = ViewForms[0];
			RightForm = ViewForms[1];
			LeftForm.Text = "Links";
			RightForm.Text = "Rechts";

			foreach (ViewForm form in ViewForms) {
				// Set up identifier labels
				form.GetNameLabel().Text = form.Text;
				// Hide label, we only need it when tweaking the shape of the movie forms
				form.GetNameLabel().Visible = false;
				// Don't show any border
				form.FormBorderStyle = FormBorderStyle.None;
				// Set up reference back to main form
				form.MainForm = this;
			}

			// Apply bounds from user preferences
			LeftForm.Bounds = Properties.Settings.Default.LeftFormBounds;
			RightForm.Bounds = Properties.Settings.Default.RightFormBounds;
		}

		private void MainForm_Load(object sender, EventArgs e) {
			// Keep view forms at user defined positions
			foreach (ViewForm form in ViewForms)
				form.StartPosition = FormStartPosition.Manual;

			// Register Hotkeys for starting and stopping movies
			RegisterHotKey(this.Handle, HOTKEY_LEFT, 0, (int)Properties.Settings.Default.LeftKey);
			RegisterHotKey(this.Handle, HOTKEY_RIGHT, 0, (int)Properties.Settings.Default.RightKey);

			// Update hotkeys in main form explanation label
			ExplanationLabel.Text = ExplanationLabel.Text.Replace("[left_key]", Properties.Settings.Default.LeftKey.ToString());
			ExplanationLabel.Text = ExplanationLabel.Text.Replace("[right_key]", Properties.Settings.Default.RightKey.ToString());

			// Load the previously selected movie folders
			LoadMediaFolders();
		}

		// Handle Windows messages
		protected override void WndProc(ref Message m) {
			// Handle hotkey message
			if (m.Msg == WM_HOTKEY) {
				int id = m.WParam.ToInt32();

				// We should only be receiving our configured play/stop left and right movie hotkeys
				Debug.Assert(id == HOTKEY_LEFT || id == HOTKEY_RIGHT);

				// Ignore hotkeys whilie we're not running
				if (!IsStarted)
					return;

				// Don't do anything if we don't have any movies to play. The movie list can in principle be switched mid-session, so it's best to check every time
				if (FileList.Items.Count == 0)
					return;

				// Check which form has been selected
				ViewForm form;
				if (id == HOTKEY_LEFT) {
					form = LeftForm;
				}
				else {
					Debug.Assert(id == HOTKEY_RIGHT);
					form = RightForm;
				}

				// Toggle start/stop
				if (form.IsPlaying) {
					form.Stop();
				}
				else {
					// Make sure an item is selected
					if (FileList.SelectedIndex == -1)
						FileList.SelectedIndex = 0;

					string filename = (string)FileList.SelectedItem;

					form.Play(filename);

					// Prepare next item in list
					FileList.SelectedIndex = (FileList.SelectedIndex + 1) % FileList.Items.Count;
				}
			}

			// Pass event on to base class
			base.WndProc(ref m);
		}

		// Save the currently selected list of movie folders to the preferences
		private void SaveMediaFolders() {
			Properties.Settings.Default.MediaFolders = MediaFoldersList.Items.Cast<string>().ToArray();
			Properties.Settings.Default.Save();
		}

		// Load the desired movie folders from the preferences into the UI
		private void LoadMediaFolders() {
			if (Properties.Settings.Default.MediaFolders != null)
				MediaFoldersList.Items.AddRange(Properties.Settings.Default.MediaFolders);

			if (Properties.Settings.Default.LastFolder != "" && MediaFoldersList.Items.Contains(Properties.Settings.Default.LastFolder))
				MediaFoldersList.SelectedItem = Properties.Settings.Default.LastFolder;
			else if (MediaFoldersList.Items.Count > 0)
				MediaFoldersList.SelectedIndex = 0;
		}

		// Show all available files when a folder is selected
		private void LoadFolder(string inFolder) {
			string[] files = Directory.GetFiles(inFolder);

			// Supported media types are hardcoded for now. Maybe there's a nice way to query Windows Media Player, or Windows, but I haven't bothered.
			string[] supported_types = new string[] { ".jpg", ".avi", ".wmv", ".png", ".gif", ".bmp", ".mp4", ".mkv" };

			// Show in file list control
			FileList.Items.Clear();
			foreach (string file in files)
				if (supported_types.Contains(Path.GetExtension(file).ToLower()))
					FileList.Items.Add(file);

			// Select first item
			if (FileList.Items.Count > 0)
				FileList.SelectedIndex = 0;
		}

		// Add a folder with media to the list
		private void AddFolderButton_Click(object sender, EventArgs e) {
			if (MediaFolderBrowserDialog.ShowDialog() == DialogResult.OK) {
				string folder = MediaFolderBrowserDialog.SelectedPath;

				// Load all media in the list control
				LoadFolder(folder);

				// Check if there are any movies/etc. in the deisred folder
				if (FileList.Items.Count > 0) {
					// Check if we don't already have this folder
					if (!MediaFoldersList.Items.Contains(folder)) {
						// Add to list, and save preferences
						MediaFoldersList.Items.Add(folder);
						SaveMediaFolders();
					}
				}
				else {
					MessageBox.Show("De map " + folder + " bevat geen geschikte plaatjes of video's");
				}
			}
		}

		// Start running
		public void Start() {
			// Make sure settings are closed
			if (ScreenSettingsForm != null)
				ScreenSettingsForm.Close();

			// Change start/stop button caption
			StartStopButton.Text = "&Stop";

			// Show label with hotkeys
			ExplanationLabel.Visible = true;

			// Disable changing the configuration while running
			ConfigurationMenuItem.Enabled = false;
			AddFolderButton.Enabled = false;
			RemoveFolderButton.Enabled = false;

			// Resync window shape from preferences
			LeftForm.Bounds = Properties.Settings.Default.LeftFormBounds;
			RightForm.Bounds = Properties.Settings.Default.RightFormBounds;

			foreach (ViewForm form in ViewForms) {
				// Bring windows to top and show
				form.TopMost = true;
				form.Show();
			}

			// Start loading the first item
			if (FileList.SelectedItem != null)
				QueueMedia((string)FileList.SelectedItem);

			// Toggle started flag
			IsStarted = true;
		}

		// Stop running
		public void Stop() {
			// Toggle start button caption
			StartStopButton.Text = "&Start";
			ExplanationLabel.Visible = false;
			ConfigurationMenuItem.Enabled = true;
			AddFolderButton.Enabled = true;
			RemoveFolderButton.Enabled = true;

			// Hide the movie windows
			foreach (ViewForm form in ViewForms)
				form.Hide();

			IsStarted = false;
		}

		private void StartButton_Click(object sender, EventArgs e) {
			if (IsStarted)
				Stop();
			else
				Start();
		}

		private void MainForm_KeyPress(object sender, KeyPressEventArgs e) {
			// Stop when Escape is pressed
			if (e.KeyChar == (char)Keys.Escape && IsStarted)
				Stop();
		}

		private void MediaFoldersList_SelectedIndexChanged(object sender, EventArgs e) {
			// Switch movie folder
			if (MediaFoldersList.SelectedItem != null) {
				// Load movies etc. in the folder into the media list
				LoadFolder((string)MediaFoldersList.SelectedItem);

				// Update preferences
				Properties.Settings.Default.LastFolder = (string)MediaFoldersList.SelectedItem;
			}
			else {
				Properties.Settings.Default.LastFolder = "";
			}

			// Save preferences
			Properties.Settings.Default.Save();
		}

		private void RemoveFolderButton_Click(object sender, EventArgs e) {
			// Remove media folder from list
			if (MediaFoldersList.SelectedItem != null) {
				MediaFoldersList.Items.Remove(MediaFoldersList.SelectedItem);
				SaveMediaFolders();
			}
		}

		private void pasvensterpositiesAanToolStripMenuItem_Click(object sender, EventArgs e) {
			// Modify the positions of the view forms
			ScreenSettingsForm = new ScreenSettingsForm();
			ScreenSettingsForm.MainForm = this;
			ScreenSettingsForm.ShowDialog();
		}

		private void overToolStripMenuItem_Click(object sender, EventArgs e) {
			// Show the about box
			AboutBox about_box = new AboutBox();
			about_box.ShowDialog();
		}

		private void PreferencesMenuItem_Click(object sender, EventArgs e) {
			// Show the preferencs window
			PreferencesForm preferences_form = new PreferencesForm();

			// Copy current settings to the preferences window
			preferences_form.LeftKey = Properties.Settings.Default.LeftKey;
			preferences_form.RightKey = Properties.Settings.Default.RightKey;
			preferences_form.MuteCheckBox.Checked = Properties.Settings.Default.Mute;

			if (preferences_form.ShowDialog() == DialogResult.OK) {
				// Copy preferences window's settings back to current settings and save
				Properties.Settings.Default.LeftKey = preferences_form.LeftKey;
				Properties.Settings.Default.RightKey = preferences_form.RightKey;
				Properties.Settings.Default.Mute = preferences_form.MuteCheckBox.Checked;
				Properties.Settings.Default.Save();

				// Update Windows hotkey binding
				UnregisterHotKey(this.Handle, HOTKEY_LEFT);
				UnregisterHotKey(this.Handle, HOTKEY_RIGHT);
				RegisterHotKey(this.Handle, HOTKEY_LEFT, 0, (int)preferences_form.LeftKey);
				RegisterHotKey(this.Handle, HOTKEY_RIGHT, 0, (int)preferences_form.RightKey);
			}
		}

		void QueueMedia(string inFilename) {
			// Hack, load 'em both, since I don't really trust the WindowsMediaPlayer control with doing clever things
			LeftForm.QueueMedia(inFilename);
			RightForm.QueueMedia(inFilename);
		}

		private void FileList_SelectedIndexChanged(object sender, EventArgs e) {
			// If the selected movie is automatically advanced, or selected by the user, queue the next movie
			if (IsStarted && FileList.SelectedItem != null)
				QueueMedia((string)FileList.SelectedItem);
		}
	}
}
