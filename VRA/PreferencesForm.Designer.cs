﻿namespace VRA {
	partial class PreferencesForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.OKButton = new System.Windows.Forms.Button();
			this.CancelButton1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.LeftKeyTextBox = new System.Windows.Forms.TextBox();
			this.RightKeyTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.MuteCheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// OKButton
			// 
			this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OKButton.Location = new System.Drawing.Point(13, 227);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(94, 23);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "&OK";
			this.OKButton.UseVisualStyleBackColor = true;
			// 
			// CancelButton1
			// 
			this.CancelButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelButton1.Location = new System.Drawing.Point(180, 227);
			this.CancelButton1.Name = "CancelButton1";
			this.CancelButton1.Size = new System.Drawing.Size(92, 23);
			this.CancelButton1.TabIndex = 1;
			this.CancelButton1.Text = "&Annuleren";
			this.CancelButton1.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(10, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(121, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Sneltoetsen voor media:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 26);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Linkerscherm:";
			// 
			// LeftKeyTextBox
			// 
			this.LeftKeyTextBox.Location = new System.Drawing.Point(93, 26);
			this.LeftKeyTextBox.Name = "LeftKeyTextBox";
			this.LeftKeyTextBox.ReadOnly = true;
			this.LeftKeyTextBox.Size = new System.Drawing.Size(179, 20);
			this.LeftKeyTextBox.TabIndex = 4;
			this.LeftKeyTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LeftKeyTextBox_MouseClick);
			// 
			// RightKeyTextBox
			// 
			this.RightKeyTextBox.Location = new System.Drawing.Point(93, 52);
			this.RightKeyTextBox.Name = "RightKeyTextBox";
			this.RightKeyTextBox.ReadOnly = true;
			this.RightKeyTextBox.Size = new System.Drawing.Size(179, 20);
			this.RightKeyTextBox.TabIndex = 6;
			this.RightKeyTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RightKeyTextBox_MouseClick);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 52);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(82, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Rechterscherm:";
			// 
			// MuteCheckBox
			// 
			this.MuteCheckBox.AutoSize = true;
			this.MuteCheckBox.Checked = true;
			this.MuteCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.MuteCheckBox.Location = new System.Drawing.Point(16, 87);
			this.MuteCheckBox.Name = "MuteCheckBox";
			this.MuteCheckBox.Size = new System.Drawing.Size(157, 17);
			this.MuteCheckBox.TabIndex = 7;
			this.MuteCheckBox.Text = "Onderdruk geluid bij filmpjes";
			this.MuteCheckBox.UseVisualStyleBackColor = true;
			// 
			// PreferencesForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.MuteCheckBox);
			this.Controls.Add(this.RightKeyTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.LeftKeyTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.CancelButton1);
			this.Controls.Add(this.OKButton);
			this.KeyPreview = true;
			this.Name = "PreferencesForm";
			this.Text = "Voorkeuren";
			this.Load += new System.EventHandler(this.PreferencesForm_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PreferencesForm_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button CancelButton1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox LeftKeyTextBox;
		private System.Windows.Forms.TextBox RightKeyTextBox;
		private System.Windows.Forms.Label label3;
		public System.Windows.Forms.CheckBox MuteCheckBox;
	}
}